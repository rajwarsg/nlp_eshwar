#download nltk libraries 
#import nltk
#nltk.download() # select option you want by default you can select 'all' since all libraries are small.

# web crawl
import urllib
response = urllib.urlopen('http://php.net/')
html = response.read()
#print (html) this will return whole webpage with html tag
from bs4 import BeautifulSoup
soup = BeautifulSoup(html,"html5lib") #install html5lib before 'pip install html5lib'
text = soup.get_text(strip=True)
#print(text) #this will return all text in the webpage
#convert test into token
tokens = [t for t in text.split()] 
#print (tokens) this will return text in array as tokens
#count word frequency by using nltk FreqDist()
import nltk
freq = nltk.FreqDist(tokens)
# making whole script encoding as utf-8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
#import matplotlib # this will give you details in graph you need to install by 'pip install matplotlib'
##stop words
from nltk.corpus import stopwords
sr = stopwords.words('english')
clean_tokens = tokens[:]
for token in tokens:
  if token in stopwords.words('english'):
    clean_tokens.remove(token)
for key,val in freq.items(): 
    print (str(key) + ':' + str(val))


##Tokenize
#You can tokenize paragraphs to sentences and tokenize sentences to words according to your needs.

from nltk.tokenize import sent_tokenize #tokenize sentence 
from nltk.tokenize import word_tokenize #tokenize word
mytext = "Hello Mr. Adam, how are you? I hope everything is going well. Today is a good day, see you dude." 
print("------------------TOKENIZE-----------------")
print(mytext)
print(sent_tokenize(mytext))
print(word_tokenize(mytext))



##Synonyms
#Synonyms of word
from nltk.corpus import wordnet #import used for synonyms and antonyms
syn = wordnet.synsets("pain")
print("------------------Synonyms-----------------")
print("pain"+ " "+ "Definition")
print(syn[0].definition())
print("pain"+ " "+ "examples")
print(syn[0].examples())

#synonyms of words of collection
synonyms = []
print("Computer")
print("List of sysnonyms")
for syn in wordnet.synsets('Computer'):
    for lemma in syn.lemmas():
        synonyms.append(lemma.name())
print(synonyms)

## Antonyms
# from nltk.corpus import wordnet
print("------------------Antonyms-----------------")
print("small")
print("List of antonyms")
antonyms = []
for syn in wordnet.synsets("small"):
    for l in syn.lemmas():
        if l.antonyms():
            antonyms.append(l.antonyms()[0].name())
print(antonyms)

##Stemming
# Word stemming means removing affixes from words and returning the root word
from nltk.stem import PorterStemmer #import Porter stemming algorithm for english
stemmer = PorterStemmer()
print("------------------Stemming-----------------")
print("Working stem English")
print(stemmer.stem('working'))

from nltk.stem import SnowballStemmer # stem function for non-english
print("List of non-english languages support")
print(SnowballStemmer.languages)
french_stemmer = SnowballStemmer('french')
print(french_stemmer.stem("French word"))

##Lemmatizing
#Word lemmatizing is similar to stemming, but the difference is the result of lemmatizing is a real word
from nltk.stem import WordNetLemmatizer #import 
lemmatizer = WordNetLemmatizer()
print("------------------Lemmatizer-----------------")
print("Lemmatize increases")
print(lemmatizer.lemmatize('increases'))
print(lemmatizer.lemmatize('increases', pos="v"))
print(lemmatizer.lemmatize('increases', pos="n")) 
print(lemmatizer.lemmatize('increases', pos="a")) 
print(lemmatizer.lemmatize('increases', pos="r"))

