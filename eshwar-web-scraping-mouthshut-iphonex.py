from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import re
import pandas as pd
import os
from tabulate import tabulate
import pdb

#launch url
url = "https://www.mouthshut.com/mobile-phones/Apple-iPhone-X-reviews-925917492"

# create a new Chrome session
driver = webdriver.Chrome('PATH/OF/CHROMEDRIVE')
driver.implicitly_wait(30)
driver.get(url)

#After opening the url above, Selenium hands the page source to Beautiful Soup
soupHomePage=BeautifulSoup(driver.page_source, "html.parser")

#Get paginatio count
pagination = soupHomePage.find(id="spnPaging")
all_link = pagination.findAll('a')
last_div = None
page_count = 0

#Get last count
for last_div in all_link:pass
if last_div:
    page_count = last_div.getText()

page_count_int = int(page_count)

print("Total page:"+ str(page_count))

#Assign return variables 
rating_arr = []
title = []
review_url = []
user_r = []

#iterate each page for product review
for page in range(1, page_count_int+1):
    url = "https://www.mouthshut.com/mobile-phones/Apple-iPhone-X-reviews-925917492-page-" + str(page)

    driver.get(url)
    print("Page No:"+ str(page))
    print("Url:"+ str(url))

    soupPage=BeautifulSoup(driver.page_source, "html.parser")
    
    #Get review portion on 
    page_content = soupPage.find(id="dvreview-listing")
    review_content = page_content.findAll(class_="review-article")
    
    #Iterate user_view on that page
    for user_review in review_content:
        main_content = user_review.find(class_="col-10 review")
        user_title = main_content.find("strong").getText() # Get user review title
        rating = len(main_content.findAll(class_="rated-star")) #Get ration of that review

        #Loading read more option to get full review.
        raw_review = main_content.find(class_="more reviewdata")
        link = re.findall(r'https://www.mouthshut.com/review/Apple-iPhone-X-review-[a-z]*', raw_review.prettify())[0]
        driver.get(link)

        soupReviewReadPage=BeautifulSoup(driver.page_source, "html.parser")

        #Get full review 
        user_read_review = soupReviewReadPage.find(class_="rev-main-content").get_text()

        #Assigning all
        rating_arr.append(rating)
        title.append(user_title)
        review_url.append(link)
        user_r.append(user_read_review)

#quit selenium
driver.quit()

print("Data ready preparing json process started ")
#combine all pandas dataframes in the list into one big dataframe
result = pd.concat([pd.DataFrame({'review_url': review_url,'Rating': rating_arr, 'Title': title, 'Full review': user_r})],ignore_index=True)

#convert the pandas dataframe to JSON
json_records = result.to_json(orient='records')

#pdb.set_trace()

#Get path for this folder
path = os.getcwd()

#open, write, and close the file
f = open(path + "\\mouthshut-iphone-x-review.json","w") #FHSU
f.write(json_records)
f.close()
